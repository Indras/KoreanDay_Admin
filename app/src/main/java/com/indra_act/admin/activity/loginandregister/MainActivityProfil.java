package com.indra_act.admin.activity.loginandregister;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.indra_act.admin.R;
import com.indra_act.admin.activity.uploadImage.MainActivityFoto;
import com.indra_act.admin.model.User;
import com.indra_act.admin.util.PrefUtil;


public class MainActivityProfil extends AppCompatActivity {

    private TextView email;
    private Button btnLogout;
    private TextView firstname;
    private TextView lastname;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivityProfil.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_profil);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView btn1 = (TextView) findViewById(R.id.lupapw);

        btn1.setOnClickListener(new View.OnClickListener() {


            public void onClick(View arg0) {
                Intent a = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(a);
            }
        });

        ImageView imageView = (ImageView) findViewById(R.id.fotoprofil);
        imageView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
                Intent a = new Intent(getApplicationContext(), MainActivityFoto.class);
                startActivity(a);
            }
        });

        email = (TextView) findViewById(R.id.email);
        btnLogout = (Button) findViewById(R.id.btn_logout);
        firstname = (TextView) findViewById(R.id.firstname);
        lastname = (TextView) findViewById(R.id.lastname);

        User user = PrefUtil.getUser(this, PrefUtil.USER_SESSION);

        email.setText(user.getData().getEmail());
        firstname.setText(user.getData().getFirstname());
        lastname.setText(user.getData().getLastname());

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutAct();

                LoginActivity.start(MainActivityProfil.this);
                MainActivityProfil.this.finish();
            }
        });

    }

    void logoutAct() {
        PrefUtil.clear(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
