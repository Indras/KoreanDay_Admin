package com.indra_act.admin.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indra_act.admin.R;
import com.indra_act.admin.activity.loginandregister.MainActivityProfil;
import com.indra_act.admin.fragment.Fragment1;
import com.indra_act.admin.fragment.Fragment2;
import com.indra_act.admin.fragment.Fragment3;
import com.indra_act.admin.fragment.Fragment4;
import com.indra_act.admin.model.User;
import com.indra_act.admin.util.PrefUtil;


@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        User user = PrefUtil.getUser(this, PrefUtil.USER_SESSION);

        View header = ((NavigationView)findViewById(R.id.nav_view)).getHeaderView(0);
        ((TextView) header.findViewById(R.id.nameView)).setText(user.getData().getFirstname());
        ((TextView) header.findViewById(R.id.emailView)).setText(user.getData().getEmail());

        ImageView imageView = header.findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {


            public void onClick(View arg0) {

                // TODO Auto-generated method stub

                Intent a = new Intent(getApplicationContext(), MainActivityProfil.class);


                startActivity(a);


            }

        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_camera);
        navigationView.setNavigationItemSelectedListener(this);

        if (findViewById(R.id.item_detail_container) != null) {
            mTwoPane = true;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment1 fragment = new Fragment1();
        fragmentManager.beginTransaction().replace(R.id.first_container, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.setting) {
           SettingActivity.start(MainActivity.this);
           return true;
        }

        else if (id == R.id.about) {


        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.nav_camera) {
            fragment = new Fragment1();
            fragmentManager.beginTransaction().replace(R.id.first_container, fragment).commit();

        } else if (id == R.id.nav_gallery) {
            fragment = new Fragment2();
            fragmentManager.beginTransaction().replace(R.id.first_container, fragment).commit();

        } else if (id == R.id.nav_slideshow) {
            fragment = new Fragment3();
            fragmentManager.beginTransaction().replace(R.id.first_container, fragment).commit();

        } else if (id == R.id.nav_manage) {
            fragment = new Fragment4();
            fragmentManager.beginTransaction().replace(R.id.first_container, fragment).commit();

        } else if (id == R.id.nav_share) {
            AboutUsActivity.start(MainActivity.this);
            return true;

        } else if (id == R.id.nav_send) {
            PrivacyPolicyActivity.start(MainActivity.this);
            return true;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }
}
